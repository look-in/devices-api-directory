The device API is built according to the REST scheme, but is significantly simplified.

This API is universal for all devices and allows you to both manage and receive information about the state of the device via Wi-Fi.
This protocol also allows using any of the LOOKin devices in existing Smart Home systems by connecting it to the controller as an HTTP device.

<div class="note">
Please note that the developed API does not always comply with the REST scheme, this is a design error, in future versions of the API it is planned to update the API to REST compliance release by release
</div>

GET requests are used to get any information.

POST/PUT requests are used to save or change data.
PUT requests cannot create data.
PUT/POST requests are not available for all data, as some fields are immutable.
This requests should be sent as JSON in the body of the message.

Requests must be interpreted regardless of the closing slash “/”. When interpreted, all request URIs are lowercase.

When checking the parameter keys to match them, both the internal name and the name passed to the API must be lowercase.

For example, requests seems like "/Device/" or "/device/" during parsing will be converted to the following form: "/device".

Request with this form

```
POST /Device 
{ "Name" : "Kitchen light"}
```
 
Will be interpret as:

```
POST /device 
{ "name" : "Kitchen light" }
```
 
All fields of the first-level object, in this case “name”, are additionally transferred to the requests inside the device as parameters, along with the parameters of the GET request, separated by &. This leads to the fact that for the device the list of parameters of the following queries is equivalent:

```
GET /device?name=Торшер
POST /Device { "Name" : "Торшер"}
```
 
Used query types:

```
GET - get information from endpoint or made simple action  
POST - update or set information 
DELETE - remove object or parameter
```

Answer codes:

```
200 - OK  
400 - Some mistake in query  
500 - Inner device error
```
 
**All requests must be sent with Content-Type: Charset = UTF-8!**

# Device UDP basics

The UDP server built into the device listens on port 61201. 
In the case if you send a packet from another port, the device will remember the sender port and will also send packets to this port when sending broadcast messages. 
In total, the device can store up to 10 additional ports, and when message will be received from 11-th port, the first saved one will be overwritten.

## Alive!

This packet is broadcasted every time the device receives power and starts up, or when it goes to the Wi-Fi network. Also, this packet is sent in a situation when the IP address of the device changes.

The request should be sent after the device is ready to process incoming http (s) requests to it on the IP and port from which the request came.

Datagram of the message:

```
LOOKin:Alive! {device id}: {type in Hex}: {power type}: {device IP}: {automation version}: {memory version}
```

Or, for example:

```
LOOKin:Alive!AA45B78C:03:01:192.154.11.8:0000F234:2000
```

## Discover!

This packet is sent broadcast every time one of the devices needs to get information about the others in the network.

The device can either initiate this request or receive it.

If a request is received without a parameter, each device that receives it responds to it.

If the request is received with a parameter, then the response must be sent only by the device whose ID is specified in the datagram.

The request should be sent after the device is ready to process incoming http (s) requests to it on the IP and port from which the request came.

Request datagram:

```
LOOKin:Discover!
```

Datagram of the response:  
```
LOOKin:Alive!AA45B78C:03:01:192.154.11.8:0000F234:2000
```

This request can also contain the ID of the searched device in the datagram:

```
LOOKin:Discover!AA45B78C
```

Datagram of a response to a request with a parameter:

`LOOKin:Alive! {device id}: {type code}: {device IP}`

## Updated!


This packet is sent broadcast every time when the sensor state of one of the devices changes or internal service state was changed. This request serves the purpose of updating the state of the sensor for one or more control devices in the network.

Please keep in mind that there 2 different Updated! datagram semantics: one for sensors, second one for services.

### Updated! sent when sensors change

[Detailed sensor details](#about_sensors)

Datagram of the message:

```
LOOKin:Updated! {device id}: {sensor id}:{event id}:{optional value}
```

or

```
LOOK.in:Updated! {device id}: {service name}:{value}:{optional value2}
```

For example:

```
LOOKin:Updated!AA45B78C:81:0:1
```

**Examples:**

LOOKin Remote catch IR signal recognized as NECx (protocol ID = 0x04) protocol with command 00012345.
This is how the packet sent by the device will looks like:
```
LOOKin:Updated!XXXXXXXX:87:04:00012345
```

LOOKin Remote catch unrecognized IR signal:

```
LOOKin:Updated!XXXXXXXX:87:FF
```

LOOKin Remote2 meteo sensor changed

```
LOOKin:Updated!XXXXXXXX:FE:00:01000154
```

For a meteo sensor (ID 0xFE), the value contains the value of temperature and relative humidity.

The resulting value 01000154 contains the temperature and humidity in hexadecimal \* 10.

Temperature: 0x0100 = 256 = 25.6 degrees
Humidity: 0x0154 = 340 = 34%

### Updated! for services

#### MQTT Service

The sensor ID can also be a string, in which case it should be interpreted as the name of the device service.

```
LOOKin:Updated!AA45B78C:MQTT:2
```

This packet means that local mqtt client was successful connected.  
Avaliable MQTT statuses:  
- Unactive = 0  
- Connecting = 1  
- Connected = 2  
- Error = 3

#### Data service

Data service sended in 2 cases:

1.  PUT/POST query to /data/ was made

```
LOOKin:Updated!AA45B78C:Data:
```

This means that local remote with given UUID was changed. For example changed name of the local remote

2. PUT/POST query to /data was made

```
LOOKin:Updated!AA45B78C:Data::
```

This means function with given ID was replaced/modified in local remote with given UUID. For example changed/added function with id 01 for local remote with UUID = ABC1

```
LOOKin:Updated!AA45B78C:Data:ABC1:01
```

## BLEAction!

This packet is sent broadcast every time a device plans to dispose of a BLE device in order to initiate a scenario on its behalf. This request informs other devices that the script has been accepted for execution and no longer needs to be executed.

Datagram of the message:  
```
LOOKin:Updated! {device id}: {scenario id}
```

# /device

## GET /device

Get device information

<!--[Open Request](https://www.postman.com/lookin/request/11774062-c2f75b44-56d3-463b-a91a-004427263f22)  -->

### Remote2 /device response
```json
{
    "Type": "Remote", 
    "Status": "Running", 
    "ID": "00000002",
    "Name": "", 
    "Time": "1592501407",
    "Timezone": "+5",
    "PowerMode": "5v",
    "CurrentVoltage": "5889",
    "Firmware": "1.37",
    "Temperature": "65",
    "BluetoothMode": "public",
    "SensorMode": "0"
}
```

### Plug /device response
```json
{
    "Type": "Plug", 
    "Status": "Running", 
    "ID": "00000002",
    "Name": "", 
    "Time": "1592501347",
    "Timezone": "+5",
    "PowerMode": "220v",
    "CurrentVoltage": "0",
    "Firmware": "2.21",
    "Temperature": "60",
    "BluetoothMode": "public",
    "SensorMode": "0"
}
```


Every parameter from answer can be get in plain\text.
For example: `http://<ip>/device/name` or `http://<ip>/device/id` will return requested parameter

## POST /device

Update device information

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-4cc96afa-1359-4c40-aeed-224ed1de8ce3)  -->

Available parameters (all are optional):
- **name** device name to set
- **time** UTC timestamp
- **timezone** current device timezone
- **sensormode** set if sensor mode is on or not
- **bluetoothmode** set BLE mode, can be private or public
- **firmware** firmware to set, can be version string like "2.37" or full http address to firmware bin


Request Headers: `Content-Type: text/plain; charset=utf-8`

### Set device name
```json
	{
		"name"			: "Kitchen device",
	}
```

### Set device time / timezone
```json
	{
		"time"			: "1592501407",
		"timezone"		: "+5",
	}
```

### Update firmware by url 
```json
	{
		"firmware"		: "http://download.look-in.club/firmwares/1.37/firmware.bin"
	}
```

# /network

Network endpoints designed to store, receive and display network information.

## GET /network

Get network information

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-a66339a4-c84e-4f2f-a512-21c1b711f950)  -->

```json
{
	"Mode": "Client",
	"IP": "192.168.1.33",
	"CurrentSSID": "testssid",
	"SavedSSID": [
	  "ssid1",
	  "ssid2"
	],
	"Map": [
		{
			"ID": "00000002",
			"IP": "192.168.1.21",
			"IsActive": "0",
			"Type": "Remote"
		},
		{
			"ID": "00000040",
			"IP": "192.168.1.36",
			"IsActive": "0",
			"Type": "Duo"
		}
	]
}
```

Explanation:
- **mode**, wi-fi mode, can be "Client" or "AP"
- **IP**, current IP address
- **CurrentSSID**, current hotspot SSID if connected
- **SavedSSID**, array of saved hotspot credentials (displayed SSID only)
- **Map**, LOOKin devices in this local network founded by this device

Each **map** item contains:
- **ID**, founded device ID
- **IP**, founded device IP
- **IsActive**, is this device online or not.
- **Type**, string device type, for example "Remote" or "Duo"
    
Every parameter from answer can be get in plain\text.
For example: `http://<ip>/network/mode` will return requested wi-fi mode

## GET /network/scannedssidlist

Get nearby hotspots list

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-1f5a019b-edfa-4d6c-b034-3af3bbddd8e7)  -->

At the time of launch, the device scans and searches for Wi-Fi networks nearby.  
This request allows user to see founded wi-fi networks

```json
[
	"SomeAP1",
	"SomeAP2",
	"SomeAP3",
	"SomeAP4",
	"SomeAP5"
]
```


## GET /network/savedssid

Get all saved hotspots list. Only SSID is shown
Saved access points are those for which data is saved by the user.

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-4268ecf9-8154-42ec-83dc-09973f462d07) -->

```json
[
	"ssid1",
	"ssid2"
]
```

## POST /network

Save or update Wi-Fi credentials

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-01a5565b-1d86-496a-a764-44bb07cfbc73) -->

### Add new Wi-Fi hotspot credentials

```json
{
	"WiFiSSID": "ssid",
	"WiFiPassword": "password"
}
```

## DELETE /network/savedssid

Delete saved hotspot from saved

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-d233a62d-d4ef-4eff-acec-4645dee31bc4) -->


### Delete hotspot with SSID "ssid1"

`DELETE /network/savedssid/**SSID**`


- **SSID**, Wi-Fi SSID to connect if credentials for this Access Point are set


## GET /network/connect

Connect to Wi-Fi hotspot

Device compares the list of saved data of Wi-Fi access points and scanned access points. If matches are found, the device connects to the access point with the highest signal strength.

In this request, you can also specify the SSID of the access point to which you want to connect device (see sample request)
In examples also you can find example how to conenct to hotspot with hidden SSID.


<!-- [Open Request](https://www.postman.com/lookin/request/11774062-005eb719-3714-472f-a5d8-1544f4bdd5ad) -->


### Connect to any avaliable Wi-Fi access point

`GET /network/connect`

### Connect to any avaliable Wi-Fi access point

`GET /network/connect/**SSID**`


- **SSID**, Wi-Fi SSID to connect if credentials for this Access Point are set

### Connect to hidden SSID

`GET /network/connect/**SSID**?hidden`


- **SSID**, Wi-Fi SSID to connect if credentials for this Access Point are set


## GET /network/keepwifi

Keep Wi-Fi on
This function can be used with devices in which sensor mode is on.  
In that case device periodicaly switch on wifi for a little time (exact time based on different conditions)  
This enpoint allows to keep wi-fi on during heave operations.
Should be invoked before operations batch.

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-5fa4acd4-b5f2-4939-a8db-162298eb98e6) -->

`GET /network/keepwifi`

## GET /network/remotecontrol
<!-- [Open Request](https://www.postman.com/lookin/request/11774062-8dea2ab1-5acf-43ac-ae87-b28f133b6ece) -->

"Remote control" function management.

This endpoint helps to manage RemoteControl state.

There are 2 functions: stop and reconnect.


### Reconnect to remote server
`GET /network/remotecontrol/reconnect`

### Stop remote control client
`GET /network/remotecontrol/stop`

# /automation

Documentation not ready yet

# /storage

Documentation not ready yet

# /sensors

## About sensors<a name="about_sensors"></a>

Sensors provide information on the status of the device.

Each device may correspond several sensors or none at all.

The sensor identifier after the 1st bit matches the identifier of the executing command. This is for easier correspondence between classes. Also, a corresponding sensor must correspond to any class of the command.

[Request to get a list of device sensors](#get_sensors)

[Request to get value of current sensor](#get_sensor)

### Sensor triggering notifications

When integrating devices into an existing system, it may be necessary to receive real-time notification of sensors

This feature is provided by the fact that when the sensor state changes, the device sends a UDP broadcast packet with information about the change:

```
LOOKin:Updated!C85611AF:87:01
```

UDP packet explanation:
- **LOOKin:**, packet prefix
- **Updated!**, event description
- **87**, sensor ID, 0x87 means - IR
- **01**, sensor event - received NEC1 IR signal

Using UDP port is 61201

### Common used sensors

#### Switch (0x81)

EventID|  Description                         | Operand
-------|--------------------------------------|----------
0x00   | Switch state                         | no operand
0x01   | Switch on                            | no operand
0x02   | Switch off                           | no operand

#### MultiSwitch (0x82)

EventID|  Description                         | Operand
-------|--------------------------------------|----------
0x00   | MultiSwitch state (0 if all off or 1 if all on)  | no operand
0x10   | Channel 1 is off                     | no operand
0x11   | Channel 1 is on                      | no operand
       |                   ...                | 
0xN0   | Channel N is off                     | no operand
0xN1   | Channel N is on                      | no operand


#### Motion (0xE1)

EventID|  Description                         | Operand
-------|--------------------------------------|----------
0x00   | Motion detection state               | no operand
0x01   | Motion detected                      | no operand


#### IR (0x87)

EventID|  Description                         | Operand
-------|--------------------------------------|----------
0x00   | IR state                             | no operand
0x01   | NEC1 signal received                 | NEC1 signal in hex
0x03   | Sony signal received                 | Sony signal in hex
0x04   | NECx signal received                 | NECx signal in hex
0x05   | Panasonic signal received            | Panasonic signal in hex
0x06   | Samsung36 signal received            | Samsung36 signal in hex
0x07   | RC5 signal received                  | RC5 signal in hex
0x08   | RC5 signal received                  | RC6 signal in hex
0x14   | Aiwa signal received                 | Aiwa signal in hex
0xED   | Repeat signal received               | no operand
0xEE   | Received command, saved in Storage   | signal ID
0xEF   | Received AC unit signal              | AC state
0xFE   | Received IR signal from saved remote | Local remote UUID, function, state
0xFF   | Received unrecognized IR signal      | no operand

#### Meteo (0xFE)

EventID|  Description                         | Operand
-------|--------------------------------------|----------
0x00   | Meteo sensor state                   | no operand
0x02   | Temperature higher that              | Temp * 10 in HEX 
0x03   | Temperature lower that               | Temp * 10 in HEX 
0x05   | Humidity higher that                 | Humidity*10 in HEX 
0x06   | Humidity lower that                  | Humidity*10 in HEX 

## GET /sensors<a name="get_sensors"></a>

Get information about sensors supported by the device.

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-a6370ed2-5b97-4777-b59b-28c660522edd) -->

### Remote2 GET /sensors response
```json
[
	"IR",
	"Meteo"
]
```

### Plug GET /sensors response
```json
[
	"Switch"
]
```

## GET /sensors/<Sensor name\><a name="get_sensor"></a>

Get sensor value

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-db5d685b-f67e-4386-a737-dca0bb1ff2f8) -->

### GET /sensors/ir

```json
{
    "Protocol": "01",
    "IsRepeated": "0",
    "RepeatSignal": "",
    "RepeatPause": "0",
    "Raw": "9060 -4450 600 -530 580 -530 600 -500 610 -530 580 -530 600 -510 610 -530 580 -1640 610 -530 580 -530 610 -500 610 -1640 580 -1650 600 -530 600 -1620 600 -1640 610 -530 580 -1650 610 -1640 600 -1630 600 -1640 610 -530 580 -530 580 -530 610 -1640 610 -500 600 -530 580 -530 600 -500 600 -1650 610 -1640 580 -1640 610 -45000",
    "Signal": "00D8801E",
    "Updated": "1592606423"
}
```

- **Protocol**, IR protocol code of the signal received by the device. 0x01 - IR protocol NEC1
- **IsRepeated**, indicates does IR signal sent once or several times
- **RepeatSignal**, repeat signal if exists
- **RepeatPause**, pause between repeated signal parts
- **Raw**, Raw object contains raw timings of the received signal
- **Signal**, recognized signal with **Protocol** scheme, if any
- **Updated**, timestamp signal received date/time

Each of the values can be obtained in Plain-Text form, for example to get Signal field you can use `GET /sensors/IR/signal/Signal` query

# /commands

## About commands

Commands perform certain actions related to the device.
There are several commands can be associated with device.

Getting information about the classes of commands supported by the device.

For example, for Plug device:

Plain Text

    GET /commands
    Answer:
    ["switch", "dimmer"]

Getting possible events for a command:

Plain Text

    GET /commands/switch  
    Answer:
    ["on", "off"]  
    
    GET /commands/dimmer  
    Answer:
    ["set", "on", "off"]

### Managing device status using commands

To control the device through commands, you must use the following parameters:

*   Command or Command ID
*   Event or Event ID
*   Command operand (optional parameter)

Device state management is possible both with POST and GET requests.  
The following requests are identical and lead to the switching on Plug device.  
These requests use the "Switch" command with the "On" action:

Plain Text

    GET /commands/switch/on
    GET /commands?command=switch&action=on  
    GET /commands?command=1&action=1  
    POST /commands/switch/on
    POST /commands  
    {  
       "command": "switch",  
       "action" : "on"  
    }

In addition to the server’s response, a message about the execution or error when executing the command may be in the response to the request in the form of the following JSON on successful execution:

Plain Text

    {
      "success": "true"
    }

Or in case of fail:

Plain Text

    {
      "success": "false"
    }

### Common used Commands

#### Switch (0x01)

Event ID| Event name      | Description | Possible operands
--------|-----------------|-------------|------------------
0x01    | on              |Switch on    | no operand
0x02    | off             |Switch off   | no operand

### MultiSwitch (0x02)

Event ID| Event name      | Description             | Possible operands
--------|-----------------|-------------------------|------------------
0x01    | channel1        |Set state for channel 1  | state
0x02    | channel2        |Set state for channel 2  | state
...     |                 |                         |
0x0N    | channelN        |Set state for channel N  | state
0xFF    | all             |Switch all to state      | state

### IR (0x07)<a name="commands_ir_table"></a>
Event ID| Event name      | Description                    | Possible operands
--------|-----------------|--------------------------------|------------------
0x01    | nec1            |Send NEC1 command, 38kHz        | Signal in HEX
0x03    | sony            |Send Sony command, 38kHz        | Signal in HEX
0x04    | necx            |Send NECx command, 38kHz        | Signal in HEX
0x05    | panasonic       |Send Panasonic command, 38kHz   | Signal in HEX
0x06    | samsung36       |Send Samsung36 command, 38kHz   | Signal in HEX
0x07    | rc5             |Send RC5 command, 38kHz         | Signal in HEX
0x08    | rc6             |Send RC6 command, 38kHz         | Signal in HEX
0x14    | aiwa            |Send Aiwa command, 38kHz        | Signal in HEX
0xED    | repeat          |Repeat last sended signal       | no operand
0xEE    | saved           |Send signal saved in /storage   | Storage item ID
0xEF    | ac              |Send ac unit control signal     | AC Operand in XXXXMTFS, where XXXX - codeset, M - AC Mode, T - temperature offset over 16 degrees, F - fan mode, S - swing mode
0xFO    | prontohex       |Send signal in ProntoHEX format | http only, no operand
0xF1    | prontohex       |Send signal in ProntoHEX format with 3s block | http only, no operand
0xF2    | prontohex-repeated |Send signal in ProntoHEX with repeat from IR command settings | http only, no operand
0xFE    | localremote |Send signal from local saved remote | Operand with Remote UUID, function and param
0xFF    | raw | Send signal from raw timinigs format |  http only, no operand

### BLE (0x08)
Event ID| Event name      | Description             | Possible operands
--------|-----------------|-------------------------|------------------
0x01    | kbd_key         |Send keyboard key press  | http only
0x02    | kbd_keydown     |Send keyboard key down   | http only
0x03    | kbd_keyup       |Send keyboard key up     | http only
0x04    | kbd_key_repeat  |Repeat last sended key   | http only
0x90    | kbd_key-blocked |Set keyboard key press with 3s block  | http only
0x91    | kbd_key-repeated|Send keyboard key press with repeat from commands key  | http only

## GET /commands<a name="get_commands"></a>
Get information about commands supported by the device.

### Remote2 GET /commands response
```json
[
	"IR",
	"BLE"
]
```

### Plug GET /commands response
```json
[
	"Switch"
]
```

## Remote/Remote2 commands examples

### Send protocol command
Send command of predefined protocol, for example NEC1  
Operand must be 8 HEX symbol length

Protocol is items under 0x30 from [this table](#commands_ir_table)

`GET /commands/ir/nec1/00820000`

### Send signal in raw timings with default frequency (38KHz)
<!-- [Open Request](https://www.postman.com/lookin/request/11774062-1241f19b-68b2-45ff-b40b-bbc719d8634d) -->

`GET /commands/ir/raw/9024 -4512 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -1692 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -1692 564 -564 564 -1692 564 -564 564 -564 564 -564 564 -1692 564 -1692 564 -564 564 -1692 564 -564 564 -1692 564 -1692 564 -1692 564 -47652 9024 -2256 564 -45000`

### Send raw signal with custom frequency
<!-- [Open Request](https://www.postman.com/lookin/request/11774062-9cd9d09d-112c-49d2-b528-20cd9914f705) -->
In this example frequency set to 40000 Hz

`GET /commands/ir/raw/40000;9024 -4512 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -1692 564 -564 564 -564 564 -564 564 -564 564 -564 564 -564 564 -1692 564 -564 564 -1692 564 -564 564 -564 564 -564 564 -1692 564 -1692 564 -564 564 -1692 564 -564 564 -1692 564 -1692 564 -1692 564 -47652 9024 -2256 564 -45000`

### Send command in ProntoHEX format
<!-- [Open Request](https://www.postman.com/lookin/request/11774062-d3f3cb5b-fdd5-4252-b359-5f905a436259) -->

`GET /commands/ir/prontohex/0000 006C 0022 0002 015B 00AD 0016 0016 0016 0016 0016 0016 0016 0016 0016 0016 0016 0016 0016 0016 0016 0016 0016 0041 0016 0041 0016 0041 0016 0041 0016 0041 0016 0041 0016 0016 0016 0041 0016 0016 0016 0041 0016 0016 0016 0016 0016 0016 0016 0016 0016 0016 0016 0016 0016 0041 0016 0016 0016 0041 0016 0041 0016 0041 0016 0041 0016 0041 0016 0041 0016 0622 015B 0057 0016 0E6C`

*Spaces might be missed*

### Send command from saved IR device
<!-- [Open Request](https://www.postman.com/lookin/request/11774062-ca472130-23bf-4fbd-b3ae-0d57c2be7182) -->
`GET /commands/ir/localremote/<Operand>`

Operand for this command is 8-hex digits string
First 4 is local remote UUID  
Following 2 hex symbols is function code  
Last 2 hex symbols is function signal ID, generally - FF

For example,  
`GET /commands/ir/localremote/7D3601FF`

Operand `7D3601FF`:
- **7D36**, UUID of saved remote
- **01**, power toggle signal ID 
- **FF**, send best suitable signal

[More about local IR remotes](#about_data)

### Send command from saved IR signal (Integrations->IR Signal in the App)
<!-- [Open Request](https://www.postman.com/lookin/request/11774062-f130d2c8-abdd-4fa1-b0b4-52fade69f38e) -->

If you saved IR signal in the App (through Integrations-\>IR Signals page) you can simply send it using this query.

`GET /commands/ir/saved/10`

Where 10 is uniq IR signal number that you can see in the App signals list.

### Send command to control AC unit
<!-- [Open Request](https://www.postman.com/lookin/request/11774062-b264ee38-14b9-45f6-a2fe-ca5dd0757c9e) -->

`GET /commands/ir/ac/<Operand>`

To control your AC unit you can use simplified AC API.

To use this API it is enought to use operand contains of 8 HEX digits with this pattern CCCCMTFS

Where:
- **CCCC**, AC unit codeset
- **M**, AC unit mode
   - **0** off
   - **1** auto
   - **2** cool
   - **3** heat
   - **4** dry
   - **5** fan_only
- **T**, Temperature. Required temperature in ℃ - 16℃. If T = 0 this means 16 ℃, if T = A this means 26℃
- **F**, Fan value
   - **0** auto
   - **1** low
   - **2** mid
   - **3** high
- **S**, Swing
   - **0** stop
   - **1** move

For example `GET /commands/ir/ac/90142801` means "Setup my AC unit with codeset 9014 for cool mode 24℃. Fan auto, Swing move"

### Send BLE command
<!-- [Open Request](https://www.postman.com/lookin/request/11774062-8e6199f4-36de-4d43-a142-101b910a3fc7) -->
`GET /commands/ble/kbd_key/<BLE COMMAND>`

BLE Command may take the following values:
Media commands:
- MEDIA\_NEXT\_TRACK
- MEDIA\_PREV\_TRACK
- MEDIA\_STOP
- MEDIA\_PLAY\_PAUSE
- MEDIA\_MUTE
- MEDIA\_VOLUME\_UP
- MEDIA\_VOLUME\_DOWN
- MEDIA\_CHANNEL\_UP
- MEDIA\_CHANNEL\_DOWN

Consumer control:
- CC\_POWER
- CC\_SLEEP
- CC\_MENU
- CC\_MENU\_PICK
- CC\_BACK
- CC\_HOME
- CC\_MENU\_UP
- CC\_MENU\_DOWN
- CC\_MENU\_LEFT
- CC\_MENU\_RIGHT

Keyboard:
- KEY\_ARROW\_UP
- KEY\_ARROW\_DOWN
- KEY\_ARROW\_LEFT
- KEY\_ARROW\_RIGHT
- KEY\_BACKSPACE
- KEY\_TAB
- KEY\_RETURN
- KEY\_ESCAPE
- KEY\_INSERT
- KEY\_DELETE
- KEY\_PAGE\_UP
- KEY\_PAGE\_DOWN
- KEY\_HOME
- KEY\_END
- KEY\_CAPS\_LOCK
- KEY\_ENTER

Keyboard keys:
Any ASCII char

For example, to send VOLUME UP BLE command you need to send `GET /commands/ble/kbd_key/MEDIA_VOLUME_UP`

# /log

Documentation not ready yet

# /data

## About data<a name="about_data"></a>


This endpoints allows to manage information stored on the device.  
Deal with these endpoints differs from device to device and depends on its type

### Remote
This endpoint allows you to receive, store and update IR remotes that are stored on each device
Get all IR remotes, saved on device.
Remotes divides by remotes types and defined functions for each device type.

List of used device types:

**TV**  
Type: 0x01
Avaliable Functions:


Short name |  ID  | Meaning
-----------|------|----------
power      | 0x01 | Power toggle
poweron    | 0x02 | Power On
poweroff   | 0x03 | Power Off
mode       | 0x04 | Input mode list
mute       | 0x05 | Audio mute toggle
volup      | 0x06 | Volume increment
voldown    | 0x07 | Volume decrement
chup       | 0x08 | Channel increment
chdown     | 0x09 | Channel decrement
cursor     | 0x0C | Cursor control
menu       | 0x0D | Menu button
back       | 0x0E | Menu button
play       | 0x0F | Play/pause button

**Media**  
Type: 0x02  
Avaliable Functions:

Short name |  ID  | Meaning
-----------|------|----------
power      | 0x01 | Power toggle
poweron    | 0x02 | Power On
poweroff   | 0x03 | Power Off
mode       | 0x04 | Input mode list
mute       | 0x05 | Audio mute toggle
volup      | 0x06 | Volume increment
voldown    | 0x07 | Volume decrement

**Light**  
Type: 0x03  
Avaliable Functions:

Short name |  ID  | Meaning
-----------|------|----------
power      | 0x01 | Power toggle
poweron    | 0x02 | Power On
poweroff   | 0x03 | Power Off

**Humifier / Dehumidifier**  
Type: 0x04  
Avaliable Functions:

Short name |  ID  | Meaning
-----------|------|----------
power      | 0x01 | Power toggle
poweron    | 0x02 | Power On
poweroff   | 0x03 | Power Off
mode       | 0x04 | Input mode list


**Air Purifier**  
Type: 0x05  
Avaliable Functions:

Short name |  ID  | Meaning
-----------|------|----------
power      | 0x01 | Power toggle
poweron    | 0x02 | Power On
poweroff   | 0x03 | Power Off
mode       | 0x04 | Input mode list
speed      | 0x0B | Speed mode

**Robo vacuum cleaner**  
Type: 0x06  
Avaliable Functions:

Short name |  ID  | Meaning
-----------|------|----------
power      | 0x01 | Power toggle
poweron    | 0x02 | Power On
poweroff   | 0x03 | Power Off

**Data Device Fan**  
Type: 0x07  
Avaliable Functions:

Short name |  ID  | Meaning
-----------|------|----------
power      | 0x01 | Power toggle
poweron    | 0x02 | Power On
poweroff   | 0x03 | Power Off
swing      | 0x0A | Swing mode
speed      | 0x0B | Fan speed mode


## GET /data

### Remote
Get saved IR remotes
Get all IR remotes, saved on device
Each device presented as object with 3 fields: Type, UUID, Updated

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-74cc7dc8-1b80-48c4-99db-c6ea511458dd) -->

```json
[
	{
	  "Type": "07",
	  "UUID": "6D1B",
	  "Updated": "1596103418"
	},
	{
	  "Type": "01",
	  "UUID": "685E",
	  "Updated": "1599130475"
	},
	{
	  "Type": "EF",
	  "UUID": "6F36",
	  "Updated": "1599748069"
	},
	{
	  "Type": "05",
	  "UUID": "85EA",
	  "Updated": "1597838823"
	}
]
```

## GET /data/<ShortUUID\>

### Remote
Get saved IR device information

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-1e717aa2-66d9-445b-bc61-51008d3ef367) -->

```json
{
	"Type": "07",
	"Name": "Вентилятор",
	"Updated": "1596103418",
	"Functions": 
	[
		{
			"Name": "power",
			"Type": "single"
		}
	]
}
```

Fields:
- **Type**, [(Type of device](#about_data)
- **Name**, Name of saved device
- **Updated**, Last updated timestamp
- **Functions**, Saved functions list
- **Extra**, Misc data (for example, Codeset for AC), *optional*

## GET /data/<ShortUUID\>/<Function\>

### Remote
Get saved IR device function details

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-67a72477-15f8-4f9a-9e66-cb24a8afb6d0) -->

For example, to get information abount channel down function of the remote with UUID AB11: `GET /data/AB11/chdown`
```json
{
   "Type": "single",
   "Signals": 
   [
      {
         "ProntoHEX": "00000073000C0000002000200040002000200020002000200020002000200020002000400040002000200020002000200020004000200CA8"
      }
   ]
}
```
## POST /data
### Remote
Create new IR device data
<!-- [Open Request](https://www.postman.com/lookin/request/11774062-52c1d066-3e7e-424f-bfad-e5573afad544) -->

```json
{
   "Type": "EF",
   "Updated": "1212112",
   "Name": "Test AC",
   "UUID": "1234",
   "Extra": "1123"
}
```
- **Type**, [(Type of device](#about_data)
- **Name**, Name of saved device
- **Updated**, Last updated timestamp
- **Functions**, Saved functions list
- **Extra**, Misc data (for example, Codeset for AC), *optional*

## PUT /data/<ShortUUID\>

### Remote
Update new IR device data

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-160d97ed-803a-4fd4-a271-ae57acd9716e) -->

```json
{
   "Type": "EF",
   "Updated": "1212112",
   "Name": "Test AC",
   "Extra": "1123"
}
```
- **Type**, [(Type of device](#about_data)
- **Name**, Name of saved device
- **Updated**, Last updated timestamp
- **Functions**, Saved functions list
- **Extra**, Misc data (for example, Codeset for AC), *optional*

## POST /data/<ShortUUID\>

### Remote
Create new IR device function

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-ed35c261-689f-4831-8834-368210ab8703) -->

```json
{
   "type": "toggle",
   "signals": 
   [
      {
         "raw": 
         {
            "Frequency": "38000",
            "Signal": "8000 -4500 9000 -4500 9000 -4500 9000 -4500 9000 -4500 9000 -4500"
         }
      },
      {
         "raw": 
         {
            "Frequency": "38000",
            "Signal": "1000 -4500 9000 -4500 9000 -4500 9000 -4500 9000 -4500 9000 -4500"
         }
      }
   ]
}
```

## PUT /data/<ShortUUID\>

### Remote
Update IR device function

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-f573e539-ae86-429b-891b-afa180f55e43) -->


```json
{
   "type": "toggle",
   "signals": 
   [
      {
         "raw": 
         {
            "Frequency": "38000",
            "Signal": "8000 -4500 9000 -4500 9000 -4500 9000 -4500 9000 -4500 9000 -4500"
         }
      },
      {
         "raw": 
         {
            "Frequency": "38000",
            "Signal": "1000 -4500 9000 -4500 9000 -4500 9000 -4500 9000 -4500 9000 -4500"
         }
      }
   ]
}
```

## DELETE /data

Erase all saved IR device data

## DELETE /data/<ShortUUID\>

### Remote
Delete saved IR device

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-4c8a0b9c-1248-4621-8766-8ec5d27bcb86) -->

## DELETE /data/<ShortUUID\>/<Function\>

### Remote
Delete saved IR device function

<!-- [Open Request](https://www.postman.com/lookin/request/11774062-bba67fa7-393a-4dbd-b2ba-0c87807aa97b) -->